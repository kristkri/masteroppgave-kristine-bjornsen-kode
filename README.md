# Masteroppgave Kristine Bjørnsen Kode

Denne siden inneholder mesteparten av koden jeg har brukt i min masteroppgave i 2021

Mappen "Climatenormals" inneholder koden som ble brukt for å beregne en ny klimanormal for årene 1991-2020. 

Mappen "Comparisons" inneholder koden for å sammenligne strålingsskjermer (trehytte og plasthytte) og for å sammenligne automatiske målinger med manuelle målinger

